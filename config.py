import os


class Config():
    BASE_DIR = os.path.dirname(os.path.dirname(__file__))
    BASE_URL = "http://{block_clock_ip_address}/api/"
    ADMIN_PASSWORD = ""
    DEBUG = True
    API_AUTH = True

