import typer
import json
import requests
import logging
from datetime import datetime, timedelta
from config import Config
from requests.auth import HTTPDigestAuth
from typing import Optional
from urllib.parse import urlparse

# create typer cli-app
app = typer.Typer(help="BLOCKCLOCK mini CLI app.")
config = Config()

# logging
formatter = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
logger = logging.getLogger(__name__)
logger.setLevel("DEBUG")
log_handler = logging.FileHandler("blockclock-cli.log")
formatter = logging.Formatter(formatter)
log_handler.setFormatter(formatter)
logger.addHandler(log_handler)


# commands
@app.command()
def status():
    """ Show the BLOCKCLOCK mini status """
    cmd = "status"
    try:
        status = call_api(cmd)
        typer.echo(f"API Response: {status}")
        logger.info(f"Status Command API Response: {status}")
    except (ValueError, TypeError) as e:
        typer.echo(f"Status Error: {e}")
        logger.warning(f"Status Error: {e}")


@app.command()
def pause():
    """ Pause the BLOCKCLOCK mini from cycling the display """
    cmd = "action/pause"
    try:
        pause = call_api(cmd)
        typer.echo(f"API Response: {pause}")
        logger.info(f"Pause Command API Response: {pause}")
    except (ValueError, TypeError) as e:
        typer.echo(f"Pause Error: {e}")
        logger.warning(f"Pause Error: {e}")


@app.command()
def update(rate: Optional[int] = typer.Argument(None, help="The number of minutes to cycle per update")):
    """ Update the BLOCKCLOCK mini display rate """
    cmd = "action/update"
    if rate:
        cmd + "?rate=" + str(rate)
    try:
        update = call_api(cmd)
        typer.echo(f"API Response: {update}")
        logger.info(f"Update Command API Response: {update}")
    except (ValueError, TypeError) as e:
        typer.echo(f"Update Error: {e}")
        logger.warning(f"Update Error: {e}")


@app.command()
def pick(action: str = typer.Argument("redraw", help="Action to perform, i.e. show next, redraw or display value.")):
    """
        Pick a BLOCKCLOCK mini Display Metric and Value.
        Examples:  next, redraw, cm.markets.price, coinbase.btcusd.spot
    """
    cmd = "pick/" + action
    try:
        pick = call_api(cmd)
        typer.echo(f"API Response: {pick}")
        logger.info(f"Pick Command API Response: {pick}")
    except (ValueError, TypeError) as e:
        typer.echo(f"Pick Error: {e}")
        logger.warning(f"Pick Error: {e}")


@app.command()
def clear_menu():
    """ Clear the BLOCKCLOCK mini menu """
    cmd = "action/clear_menu"
    try:
        clr_menu = call_api(cmd)
        typer.echo(f"API Response: {clr_menu}")
        logger.info(f"Clear Menu API Response: {clr_menu}")
    except (ValueError, TypeError) as e:
        typer.echo(f"Clear Menu Error: {e}")
        logger.warning(f"Clear Menu Error: {e}")


@app.command()
def shutdown():
    """ Shutdown the BLOCKCLOCK mini """
    cmd = "action/powerdown"
    try:
        shutdown = call_api(cmd)
        typer.echo(f"API Response: {shutdown}")
        logger.info(f"Shutdown Command API Response: {shutdown}")
    except (ValueError, TypeError) as e:
        typer.echo(f"Shutdown Error: {e}")
        logger.warning(f"Shutdown Error: {e}")


@app.command()
def restart():
    """ Restart the BLOCKCLOCK mini """
    cmd = "action/reboot"
    try:
        reboot = call_api(cmd)
        typer.echo(f"API Response: {reboot}")
        logger.info(f"Restart Command API Response: {reboot}")
    except (ValueError, TypeError) as e:
        typer.echo(f"Restart Error: {e}")
        logger.warning(f"Restart Error: {e}")


@app.command()
def flash():
    """ Flash the BLOCKCLOCK mini lights """
    cmd = "lights/flash"
    try:
        flash = call_api(cmd)
        typer.echo(f"API Response: {flash}")
        logger.info(f"Flash Command API Response: {flash}")
    except (ValueError, TypeError) as e:
        typer.echo(f"Flash Error: {e}")
        logger.warning(f"Flash Error: {e}")


@app.command()
def lights(color: str):
    """
        Turn the BLOCKCLOCK mini LEDs ON
        Color: hex value (i.e. FF0000, FFFFFF, 3333FF)
    """
    cmd = "lights/" + color
    try:
        lights = call_api(cmd)
        typer.echo(f"API Response: {lights}")
        logger.info(f"Lights On Command API Response: {lights}")
    except (ValueError, TypeError) as e:
        typer.echo(f"Lights On Error: {e}")
        logger.warning(f"Lights On Error: {e}")


@app.command()
def lights_off():
    """ Turn the BLOCKCLOCK mini LEDs OFF """
    cmd = "lights/off"
    try:
        off = call_api(cmd)
        typer.echo(f"API Response: {off}")
        logger.info(f"Lights Off Command API Response: {off}")
    except (ValueError, TypeError) as e:
        typer.echo(f"Lights Off Error: {e}")
        logger.warning(f"Lights Off Error: {e}")


@app.command()
def show_text(text: str, params: Optional[str] = typer.Argument(None)):
    """
       Display arbitrary text on the BlockClock display
       Required: (text) TEXT to display
       Optional: string '{"tl": "top left text", "br": "bottom right text", "pair": "BTC/USD", "sym": "$"}'
    """
    txt = None
    cmd = "show/text/" + str(text)
    try:
        if params:
            txt = call_api(cmd, params=params)
            typer.echo(f"API Response: {text}")
            logger.info(f"Show Text Command API Response: {text}")
        else:
            txt = call_api(cmd)
    except (ValueError, TypeError) as e:
        typer.echo(f"Show Text Error: {e}")
        logger.warning(f"Show Text Error: {e}")

    return txt


@app.command()
def show_number(n: int, params: Optional[str] = typer.Argument(None)):
    """
       Display arbitrary numbers on the BlockClock display
       Required: (n) NUMBER to display
       Optional: string '{"tl": "top left text", "br": "bottom right text", "pair": "BTC/USD", "sym": "$"}'
    """
    num = None
    cmd = "show/number/" + str(n)
    try:
        if params:
            num = call_api(cmd, params=params)
            typer.echo(f"API Response: {num}")
            logger.info(f"Show Number Command API Response: {num}")
        else:
            num = call_api(cmd)
    except (ValueError, TypeError) as e:
        typer.echo(f"Show Number Error: {e}")
        logger.warning(f"Show Number Error: {e}")

    return num


@app.command()
def show_time():
    """ Show the current LOCAL time """
    st = None
    current_time = datetime.now().strftime("%I:%M%p")
    params = {
        "tl": "Current",
        "br": "Time"
    }

    try:
        st = show_text(
            current_time,
            params=json.dumps(params)
        )
        typer.echo(f"Show Time Response: {st}")
        logger.info(f"Show Time API Response: {st}")
    except (TypeError, ValueError) as e:
        typer.echo(f"Show Time Error: {e}")
        logger.warning(f"Show Time Error: {e}")

    return st


# api functions
def call_api(command: str, params: Optional[dict] = None):
    """ Call the BLOCKCLOCK mini built-in API """
    response = None
    auth = None
    timeout = 10
    method = "GET"
    hdr = {"Content-Type": "application/json"}
    url = validate_url(config.BASE_URL)
    if params is not None:
        params = json.loads(params)
    if config.API_AUTH:
        auth = HTTPDigestAuth("", config.ADMIN_PASSWORD)

    if url:
        url = config.BASE_URL

        try:
            r = requests.request(
                method,
                url + command,
                headers=hdr,
                auth=auth,
                params=params,
                timeout=timeout
            )

            if r.status_code == 200:
                response = r.json()
                logger.info(f"API Response: Status {r.status_code} OK")
            else:
                response = {
                    "error": f"HTTP Status Code: {r.status_code}"
                }
                logger.warning(f"HTTP Status Code: {r.status_code}")

        except requests.exceptions.HTTPError as http_err:
            typer.echo(f"HTTP Error: {http_err}")
            response = {
                "error": f"HTTP Error: {http_err}"
            }
            logger.warning(f"HTTP Error: {http_err}")
    else:
        response = {
            "error": "The BlockClock mini URL is invalid."
        }
        logger.critical(f"BlockClock mini URL is invalid: {config.BASE_URL}")

    return response


def validate_url(url):
    """ Validate the BLOCKCLOCK mini URL """
    try:
        result = urlparse(url)
        return all([result.scheme, result.netloc, result.path])
    except ValueError as e:
        typer.echo(f"INVALID URL: {e}")
        logger.critical(f"BLOCKCLOCK mini URL is invalid: {config.BASE_URL}")
        return False


if __name__ == "__main__":
    app()
