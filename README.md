## BLOCKCLOCK&trade; mini CLI App
#### A Python3 &amp; Typer CLI App for BLOCKCLOCK&trade; mini API

* Installation
* Configuration
* Usage
* Examples
* Automation


#### Installation

To get started, clone this repository in your project directory

I use Poetry to manage my project dependencies.  Make sure you have [Poetry](https://python-poetry.org/docs/#osx--linux--bashonwindows-install-instructions) installed and in your path.

```
~/Projects$ git clone https://gitlab.com/JamesCraigLND/blockclock-cli-app.git
~/Projects$ cd blockclock-cli-app/
~/Projects/blockclock-cli-app$
```

Next, create a virtual environment and set the Poetry environment variable to use the new Python environment's executable.

```
~/Projects/blockclock-cli-app$ python3 -m virtualenv venv
~/Projects/blockclock-cli-app$ source venv/bin/python
~/Projects/blockclock-cli-app$ poetry env use venv/bin/python
~/Projects/blockclock-cli-app$ poetry install
```

The project dependencies will install. 

> If you get an error about Poetry version conflict with Python 3.9, use poetry env use python3.9 or poetry env use /venv/bin/python3.9

Alternatively, use pip to install the two project dependencies, requests and typer.

```
~/Projects/blockclock-cli-app$ pip install requests typer
```

#### Configuration

Modify the ```config.py``` file to add the IP address of your network's BLOCKCLOCK&trade; mini interface.  Example:  http://10.0.1.100/

If you have your BLOCKCLOCK&trade; mini protected with a password, then enable the API_AUTH settings in the ```config.py``` and set to True.  Add your password to ADMIN_PASSWORD.

```
class Config(object):
    DEBUG = True
    API_AUTH = True
    ADMIN_PASSWORD = "your_password"
    BASE_URL = "http://{block_clock_ip_address}/api/"
```

#### Usage

The CLI app contains a majority of the BLOCKCLOCK&trade;'s API functionality.  To see all of the available commands, run the help for ```main.py```

```
~/Projects/blockclock-cli-app$ python main.py --help

Usage: main.py [OPTIONS] COMMAND [ARGS]...

Options:
  --install-completion [bash|zsh|fish|powershell|pwsh]
                                  Install completion for the specified shell.
  --show-completion [bash|zsh|fish|powershell|pwsh]
                                  Show completion for the specified shell, to
                                  copy it or customize the installation.
  --help                          Show this message and exit.

Commands:
  clear-menu   Clear the BLOCKCLOCK mini menu
  flash        Flash the BLOCKCLOCK mini lights
  lights       Turn the BLOCKCLOCK mini LEDs ON Color: hex value (i.e.
  lights-off   Turn the BLOCKCLOCK mini LEDs OFF
  pause        Pause the BLOCKCLOCK mini from cycling the display
  pick         Pick a BLOCKCLOCK mini Display Metric and Value.
  restart      Restart the BLOCKCLOCK mini
  show-number  Display arbitrary numbers on the BLOCKCLOCK mini display...
  show-text    Display arbitrary text on the BLOCKCLOCK mini display Required:...
  show-time    Display the LOCAL time in 12 hour format with AM/PM.
  shutdown     Shutdown the BLOCKCLOCK mini
  status       Show the BLOCKCLOCK mini status
  update       Update the BLOCKCLOCK mini display rate
```

#### Examples

Basic Commands:

```
~/Projects/blockclock-cli-app$ python main.py flash
~/Projects/blockclock-cli-app$ python main.py restart
~/Projects/blockclock-cli-app$ python main.py status
~/Projects/blockclock-cli-app$ python main.py lights FF0000
~/Projects/blockclock-cli-app$ python main.py lights-off
~/Projects/blockclock-cli-app$ python main.py pick cm.markets.price
~/Projects/blockclock-cli-app$ python main.py pick next
~/Projects/blockclock-cli-app$ python main.py update 10
```

Advanced Commands:

* show-number
* show-text 

These commands accepts a dict as string object containing the key pairs for top left, bottom right, symbol and pair

```
~/Projects/blockclock-cli-app$ python main.py show-text JamesLN '{"tl": "lightning", "br": "network", "pair": "BTC/USD", "sym": "bitcoin"}'
```

#### Automate with Cron

I use crontab to display the time every quarter hour.  I also use cron to shutdown my BLOCKCLOCK&trade; mini every night at 10:00pm.

```
*/15 8-22 * * * /home/username/Projects/blockclock-cli-app/venv/bin/python /home/username/Projects/blockclock-cli-app/main.py show-time

0 22 * * * /home/username/Projects/blockclock-cli-app/venv/bin/python /home/username/Projects/blockclock-cli-app/main.py shutdown
```

#### TODO
1.  Display Images
2.  Add Command Groups
3.  Improve API response messaging
3.  Improve Error handling

